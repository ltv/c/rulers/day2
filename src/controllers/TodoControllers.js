const { insert, deleteById, findBy, findById, update } = require("../models/todo") 

exports.createTodo = (req, res) => {
    const params = req.body
    const ret = insert(params)
    res.json(ret)
};
exports.updateTodo = (req, res) => {
    const params = req.body
    const ret = update(params)
    res.json(ret)
};
exports.getAllTodos = (req, res) => {
    const ret = findBy()
    res.json(ret)
};
exports.deleteTodo = (req, res) => {
    const {id } = req.body
    if (Number.isInteger(id)){
        const ret = deleteById(id)
        res.json(ret)
    }else {
        res.json({ error: "id must be an integer"})
    }
};
exports.clearCompletedTodo = (req, res) => {};