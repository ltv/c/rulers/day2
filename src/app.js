const express = require('express')
const router = require('./router')
const allRouters = require('express-list-endpoints')
const app = express()
const bodyParser = require('body-parser')
const port = "3000"

app.use(bodyParser.json())
app.use('/api', router)

app.listen(port, () => {
    console.log(`Example is running on port ${port}`)
    console.log(allRouters(app))
})