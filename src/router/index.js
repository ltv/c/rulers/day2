const express = require('express')
const router = express.Router()
const { createTodo, updateTodo, getAllTodos, deleteTodo } = require("../controllers/TodoControllers")

router.get('/todos', getAllTodos)
router.post('/createTodo', createTodo)
router.post('/updateTodo', updateTodo)
router.post('/deleteTodo', deleteTodo)

module.exports = router;