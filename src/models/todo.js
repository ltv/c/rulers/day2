const todos = [];

/**
 * Insert todo
 * @param {id, title, comeleted} todo
 * 
 */
exports.insert = todo => {
    const newTodo = { ...todo, completed: false }
    todos.push(newTodo);
    return newTodo
}

/**
 * Update todo
 * @param {id, title, comeleted} todo
 * @returns { list || boolean } todo || false
 */
exports.update = todo => {
    const foundIdx = todos.findIndex(t => console.log(t.id, todo.id) || t.id === todo.id)
    if (foundIdx > -1) {
        todos[foundIdx] = { ...todo };
        return todos[foundIdx]
    } else {
        return false
    }
}

/**
 * Find todo by Id
 * @param { int } id - id of the todo
 * @returns { todo || false } todo || false
 */
exports.findById = id => {
    const foundTodo = todos.find(t => t.id === id)
    if (foundTodo) {
        return foundTodo
    }
    return false
}

/**
 * Delete todo
 * @param { int } id - Id of the todo 
 */
exports.deleteById = id => {
    const todoIdx = todos.findIndex(todo => todo.id === id)
    if (todoIdx === -1) {
        return false
    }
    return todos.splice(todoIdx, 1)
}

/**
 * Find completed todo
 * @param { completed } param
 * @returns { list } foundTodo
 */
exports.findBy = params => {
    if (!params) {
        return todos
    } else {
        const { completed } = params
        console.log(completed, todos, "asdasd")
        return todos.filter(todo => todo.completed === completed)
    }
}