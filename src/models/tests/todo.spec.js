const { insert, update, findBy, findById, deleteById } = require('../todo')

describe('Test insert todo', () => {
    const firstTodo = { id: 1, title: 'First todo' }
    const secondTodo = { id: 2, title: 'Second todo' }
    const thirdTodo = { id: 3, title: 'Third todo' }
    const fourthTodo = { id: 4, title: 'Fourth todo' }
    it('should create todo', () => {
        expect.assertions(1)
        const inserted = insert(firstTodo)
        expect(inserted).toEqual({...firstTodo, completed: false })
    })
    it('should update todo', () => {
        expect.assertions(2)
        const inserted = insert(secondTodo)
        expect(inserted).toEqual({ ...secondTodo, completed: false })
        const updated = update({ ...secondTodo, completed: true })
        expect(updated).toEqual({ ...secondTodo, completed: true })
    })
    it('should find todo by id', () => {
        expect.assertions(2)
        const inserted = insert(thirdTodo)
        expect(inserted).toEqual({ ...thirdTodo, completed: false })
        const todo = findById(3)
        expect(todo).toEqual(inserted)
    })
    it('should delete todo', () => {
        expect.assertions(3)
        const todos = deleteById(1)
        expect(todos).toEqual([{...firstTodo, completed: false}])
        expect(todos.length).toEqual(1)
        expect(todos).not.toEqual(false)
    })
    it('should find todo', () => {
        const inserted = insert(fourthTodo)
        expect(inserted).toEqual({ ...fourthTodo, completed: false })
        const foundCmptedTodo = findBy({ completed: true})
        expect(foundCmptedTodo.length).toEqual(1)
        const foundUnCptedTodo = findBy({ completed: false})
        expect(foundUnCptedTodo.length).toEqual(2)
    })
})